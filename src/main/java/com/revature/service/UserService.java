package com.revature.service;

import com.revature.models.User;
import com.revature.models.UserRole;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    public List<User> getAll(){
        List<User> users = new ArrayList<User>();
        users.add(new User(53, "Lola", "lola@gmail.com", "lolarox", UserRole.PREMIUM));
        users.add(new User(54, "Harold", "harry@gmail.com", "supersecret", UserRole.BASIC));
        users.add(new User(55, "Gertrude", "trudy@gmail.com", "pass1234", UserRole.ADMIN));
        return users;
    }
}
