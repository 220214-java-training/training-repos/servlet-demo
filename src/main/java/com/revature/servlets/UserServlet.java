package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.models.User;
import com.revature.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

// class responds to requests made to /users
public class UserServlet extends HttpServlet {

    @Override // this method responds to GET requests
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // obtaining user data
        UserService userService = new UserService();
        List<User> users = userService.getAll();

        // use jackson-databind to convert java objects into JSON
        ObjectMapper om = new ObjectMapper();
        String userJson = om.writeValueAsString(users);

        try(PrintWriter pw = resp.getWriter()) {
//            pw.write(users.toString());  // instead of using the users toString, we want to use JSON
            pw.write(userJson); // write the JSON to response body
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // obtain the new user info from the request body
        try(BufferedReader reader = req.getReader();){
            String requestBody = reader.readLine(); // reads in any text in the request body
            System.out.println(requestBody);
            // store that info with the rest of our user data
            // return a response to our client
        }

    }
}
