package com.revature.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HomePageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "text/html");
        try(PrintWriter pw = resp.getWriter()){
            pw.write("<!DOCTYPE html><html><head><title>My Home Page</title></head><body><h2>Welcome to my " +
                    "App!</h2><img src=\"https://cdn.shopify.com/s/files/1/1061/1924/products/4_large" +
                    ".png?v=1571606116\" alt=\"yay\" height=200 width=200 ></body></html>");
        }
    }
}
