package com.revature.models;

public enum UserRole {

    BASIC, PREMIUM, ADMIN

}
